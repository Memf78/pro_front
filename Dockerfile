FROM node:carbon
RUN mkdir -p /front/app
WORKDIR /front/app
COPY bower.json ./
RUN apt-get update && \
    apt-get install -y --no-install-recommends git && \
    apt-get clean && \
    npm install -g gulp bower polymer-cli --unsafe-perm
RUN npm install -g generator-polymer-init-custom-build 
EXPOSE 8000
COPY index.html ./
RUN mkdir -p /src
COPY src ./src
CMD ["polymer", "serve"]
